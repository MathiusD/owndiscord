import re

def text_format_with_char(txt:str, char:str, maxi:int = 2048, delim:str = "", post_process = False):
    '''
    Fonction de découpe qui découpe le texte passé en argument en tableau contenant
    au maximum le maximum passé en argument et en respectant le découpage du
    caractère passé en argument
    '''
    tab_txt = []
    maxi = maxi - len(delim) * 2
    if len(txt) >= maxi:
        splits = txt.split(char)
        txt_coup = ''
        compt = 0
        while compt < len(txt):
            for split in splits:
                if len(txt_coup + split) >= maxi:
                    if post_process is not None:
                        for tab in post_process(txt_coup, maxi):
                            tab_txt.append("%s%s%s" % (delim, tab, delim))
                    else:
                        tab_txt.append("%s%s%s" % (delim, txt_coup, delim))
                    txt_coup = ''
                while compt < len(txt) and txt[compt] == char:
                    txt_coup =  "%s%s" % (txt_coup, char)
                    compt += 1
                txt_coup = "%s%s" % (txt_coup, split)
                compt += len(split)
        if len(txt_coup) > 0:
            tab_txt.append("%s%s%s" % (delim, txt_coup, delim))
    else:
        tab_txt.append("%s%s%s" % (delim, txt, delim))
    return tab_txt


def find_tab(txt):
    """
    Fonction de recherche d'au moins une tabulation suivi par n'importe quel autre
    caractère exemple `\t\tBlap` match et `\t\t` ne match pas
    """
    search = re.search("\t*\t[^\t]", txt)
    return search.group(0) if search else None

def text_format_with_tab(txt):
    '''
    Fonction de recherche et d'ajustement des tabulation non gérée au sein d'embed
    '''
    data = find_tab(txt)
    while data:
        temp = data
        if temp[len(temp)-1] != '-':
            prefix = '-'
        else:
            prefix = '•'
        temp = temp.replace(temp[len(temp)-2:], "%s %s" % (prefix, temp[len(temp)-1]))
        temp = temp.replace('\t', "  ")
        txt = txt.replace(data, temp)
        data = find_tab(txt)
    return txt

def text_format_with_respect_words(txt:str, maxi:int = 2048, delim:str = ""):
    '''
    Fonction de découpe qui découpe le texte passé en argument en tableau contenant
    au maximum le maximum passé en argument et en respectant le découpage des mots
    '''
    return text_format_with_char(txt, ' ', maxi, delim)

def text_format_with_respect_lines(txt:str, maxi:int = 2048, delim:str = "", with_tab:bool = True):
    '''
    Fonction de découpe qui découpe le texte passé en argument en tableau contenant
    au maximum le maximum passé en argument et en respectant le découpage des lignes
    et des mots (et des tabulations si l'option n'est pas désactivée)
    '''
    return text_format_with_char(text_format_with_tab(txt) if with_tab else txt, '\n', maxi, delim, text_format_with_respect_words)