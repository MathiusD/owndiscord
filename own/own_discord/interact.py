from own import own_discord
from discord.ext import commands
import abc

class Interact(abc.ABC):

    @classmethod
    async def send(cls, ctx:commands.Context, obj:object):
        embd = cls.createMsg(obj)
        if embd:
            return await own_discord.embed_send(ctx, embd)

    @staticmethod
    @abc.abstractmethod
    def createMsg(obj:object):
        pass

    @classmethod
    async def sendChoices(cls, ctx:commands.Context, objs:list):
        msg = "Select one :\n%s" % (cls.createList(objs))
        return await own_discord.embed_send(ctx, own_discord.embed_create(msg))

    @classmethod
    async def sendList(cls, ctx:commands.Context, objs:list):
        return await own_discord.embed_send(ctx, own_discord.embed_create(cls.createList(objs)))
    
    @staticmethod
    @abc.abstractmethod
    def createList(objs:list):
        pass
    
    @classmethod
    async def select(cls, ctx:commands.Context, objs:list):
        if len(objs) > 0:
            if len(objs) > 1:
                await cls.sendChoices(ctx, objs)
                dit = {
                    "cancel":"Cancel",
                    "max":len(objs) + 1,
                    "min":0,
                    "error_msg":"Please enter number (1 to %i)[Or Cancel for cancel search]" % len(objs)
                }
                value = await own_discord.wait_input(ctx, own_discord.int_check, dit, (len(own_discord.embed_create(cls.createList(objs))) * 60.0))
                await ctx.trigger_typing()
                if not value or value == -1:
                    await own_discord.reponse_send(ctx, "Canceled")
                else:
                    return objs[value - 1]
            else:
                return objs[0]
        else:
            await own_discord.reponse_send(ctx, "Not Found")
        return None

    @classmethod
    async def display(cls, ctx:commands.Context , data:list):
        data = await cls.select(ctx, data)
        if data:
            return await cls.send(ctx, data)
        return None