import discord
from discord.ext import commands
from own.own_discord import react

async def edit_msg(ctx:commands.Context ,msg:discord.Message, data:object):
    if isinstance(data, discord.Embed):
        auth = True
        if ctx.guild:
            bot_perms = ctx.channel.permissions_for(ctx.guild.get_member(ctx.bot.user.id))
            auth = bot_perms.embed_links == True
        if auth:
            return await msg.edit(embed=data)
    return await msg.edit(content=data)

async def msg_prec(ctx:commands.Context, msg:discord.Message, index:int, data:list, other_task:list= [], unlocked:bool = False):
    if len(data) > index > 0:
        new_index = index - 1
        if await react.wait_react(ctx, msg, "◀️", 180.0, unlocked) is True:
            await edit_msg(ctx, msg, data[new_index])
            if new_index == 0:
                await msg.remove_reaction("◀️", ctx.bot.user)
            for task in other_task:
                task.cancel()
            nex = []
            pre = []
            pre.append(ctx.bot.loop.create_task(msg_prec(ctx, msg, new_index, data, nex, unlocked)))
            nex.append(ctx.bot.loop.create_task(msg_next(ctx, msg, new_index, data, pre, unlocked)))

async def msg_next(ctx:commands.Context, msg:discord.Message, index:int, data:list, other_task:list= [], unlocked:bool = False):
    if 0 <= index < len(data) - 1:
        new_index = index + 1
        if await react.wait_react(ctx, msg, "▶️", 180.0, unlocked) is True:
            await edit_msg(ctx, msg, data[new_index])
            if new_index == len(data) - 1:
                await msg.remove_reaction("▶️", ctx.bot.user)
            for task in other_task:
                task.cancel()
            nex = []
            pre = []
            pre.append(ctx.bot.loop.create_task(msg_prec(ctx, msg, new_index, data, nex, unlocked)))
            nex.append(ctx.bot.loop.create_task(msg_next(ctx, msg, new_index, data, pre, unlocked)))

async def bind_react_and_send(ctx:commands.Context, data:list, coro, react_unlocked:bool = False):
    if len(data) > 0:
        auth = True
        if ctx.guild:
            bot_perms = ctx.channel.permissions_for(ctx.guild.get_member(ctx.bot.user.id))
            auth = bot_perms.add_reactions == True
        if auth:
            msg = await coro(data[0], data)
            if len(data) > 1:
                ctx.bot.loop.create_task(msg_next(ctx, msg, 0, data, unlocked=react_unlocked))
            return msg
        else:
            out = []
            for dat in data:
                out.append(await coro(dat, data))
                if data.index(dat) < len(data) - 1:
                    await ctx.trigger_typing()
            return out
    return None
