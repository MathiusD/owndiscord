import discord
from discord.ext import commands
from ..utils import text_format_with_respect_lines
from .react import bind_react_and_send

async def reponse_send(ctx:commands.Context, msg:str, embd:discord.Embed = None, fil:discord.File = None, delim:str = "", react_unlocked:bool = False):
    '''
    Fonction d'envoi de message avec le splitage si nécessaire ainsi que
    l'ajout d'un embed commun à tout les messages si il est renseigné
    '''
    if msg != "":
        tab = text_format_with_respect_lines(msg, 2000, delim)
        async def send(msg:str, data:list):
            auth = True
            if ctx.guild:
                bot_perms = ctx.channel.permissions_for(ctx.guild.get_member(ctx.bot.user.id))
                if embd != None:
                    auth = auth and bot_perms.embed_links == True
                if fil != None:
                    auth = auth and bot_perms.attach_files == True
            if auth:
                return await ctx.send(msg, embed = embd, file = fil)
            else:
                errorMsg = "<#%i>(#%s) doesn't allow send" % (ctx.channel.id, ctx.channel.name)
                first = True
                if embd != None and bot_perms.embed_links != True:
                    errorMsg = "%s%s embed" % (errorMsg, "," if first is False else "")
                    if first == True:
                        first = False
                if fil != None and bot_perms.attach_files != True:
                    errorMsg = "%s%s files" % (errorMsg, "," if first is False else "")
                    if first == True:
                        first = False
                errorMsg = "%s. Result send in your DM." % errorMsg
                await ctx.send(errorMsg)
                chan = ctx.author.dm_channel
                if chan is None:
                    chan = await ctx.author.create_dm()
                return await chan.send(msg, embed = embd, file = fil)
        return await bind_react_and_send(ctx, tab, send, react_unlocked)
    return None

def embed_create(txt:str, name:str = None, imageUri:str = None, colour:int = None):
    '''
    Fonction de création d'Embed, on lui passe le texte,
    l'éventuel titre, l'éventuel uri de l'image et elle retourne un
    tableau d'Embed
    '''
    tab_embed = []
    tab_txt = text_format_with_respect_lines(txt)
    for i in range(len(tab_txt)):
        embed = discord.Embed(title = name if name else "", description = tab_txt[i])
        if imageUri:
            embed.set_image(url=imageUri)
        if colour:
            embed.colour = colour
        tab_embed.append(embed)
    return tab_embed

async def prep_embed(ctx:commands.Context, embed:list):
    auth = True
    if ctx.guild:
        bot_perms = ctx.channel.permissions_for(ctx.guild.get_member(ctx.bot.user.id))
        auth = bot_perms.embed_links == True
    if auth:
        return embed
    else:
        msg = ""
        for dat in embed:
            msg = "%s%s" % (msg, dat.description)
        return text_format_with_respect_lines(msg, 2000, '```')

async def embed_update(ctx:commands.Context, msg:discord.Message, embd:list, react_unlocked:bool = False):
    '''
    Fonction de mise à jour de l'Embed d'un message
    '''
    embd = await prep_embed(ctx, embd)
    async def edit(embed, data:list):
        if isinstance(embed, discord.Embed):
            return await msg.edit(embed = embed)
        else:
            return await msg.edit(content = embed)
    return await bind_react_and_send(ctx, embd, edit, react_unlocked)

async def embed_send(ctx:commands.Context, embd:list, react_unlocked:bool = False, file:discord.File = None):
    '''
    Fonction d'envoi d'Embed
    '''
    embd = await prep_embed(ctx, embd)
    async def send(embed, data:list):
        auth = True
        sendable = ctx
        if ctx.guild:
            bot_perms = ctx.channel.permissions_for(ctx.guild.get_member(ctx.bot.user.id))
            if file != None:
                auth = auth and bot_perms.attach_files == True
        if auth is False:
            errorMsg = "<#%i>(#%s) doesn't allow send" % (ctx.channel.id, ctx.channel.name)
            first = True
            if file != None and bot_perms.attach_files != True:
                errorMsg = "%s%s files" % (errorMsg, "," if first is False else "")
                if first == True:
                    first = False
            errorMsg = "%s. Result send in your DM." % errorMsg
            await ctx.send(errorMsg)
            chan = ctx.author.dm_channel
            if chan is None:
                chan = await ctx.author.create_dm()
            sendable = chan
        if isinstance(embed, discord.Embed):
            return await sendable.send('', embed = embed, file=file)
        else:
            return await sendable.send(embed, file=file)
    return await bind_react_and_send(ctx, embd, send, react_unlocked)

async def publish(msgs = None):
    if isinstance(msgs, list):
        for msg in msgs:
            if msg.channel.type is discord.ChannelType.news:
                await msg.publish()
    elif isinstance(msgs, discord.Message) and msgs.channel.type is discord.ChannelType.news:
        await msgs.publish()
