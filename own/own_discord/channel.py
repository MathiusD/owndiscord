from discord import TextChannel


async def fetchMembers(channel: TextChannel):
    """List[:class:`Member`]: Returns all members that can see this channel."""
    members = []
    async for member in channel.guild.fetch_members(limit=channel.guild.max_members):
        if channel.permissions_for(member).read_messages:
            members.append(member)
    return members
