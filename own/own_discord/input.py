import asyncio
import discord
from discord.ext import commands
from own.own_discord import reponse_send


async def wait_input(ctx: commands.Context, funct, data_funct: dict, timeout: int = 60.0):
    try:
        return await asyncio.wait_for(coro_wait_input(ctx, funct, data_funct), timeout)
    except asyncio.TimeoutError:
        return None


async def coro_wait_input(ctx: commands.Context, funct, data_funct: dict):
    value = None
    while value == None:
        out = await ctx.bot.wait_for('message')
        value = await funct(ctx, out, data_funct)
    return value


async def int_check(ctx: commands.Context, out: discord.Message, data: dict, timeout: int = 60.0):
    try:
        return await asyncio.wait_for(coro_int_check(ctx, out, data), timeout)
    except asyncio.TimeoutError:
        return None


async def coro_int_check(ctx: commands.Context, out: discord.Message, data: dict):
    value = None
    if ctx.author.id == out.author.id and ctx.channel.id == out.channel.id:
        out = extractValueFromPingValue(ctx, out)
        cancelType = "content"
        if "cancelType" in data.keys():
            cancelType = data["cancelType"]
        if (cancelType == "casse" and out == data["cancel"]) or (cancelType == "content" and out.strip().lower() == data["cancel"].strip().lower()):
            value = -1
        else:
            try:
                out = int(out)
                value = out if data["max"] > out > data["min"] else None
            except Exception:
                pass
            if value == None:
                await reponse_send(ctx, data["error_msg"])
    return value


async def str_check(ctx: commands.Context, out: discord.Message, data: dict):
    value = None
    if ctx.author.id == out.author.id and ctx.channel.id == out.channel.id:
        out = extractValueFromPingValue(ctx, out)
        cancelType = "content"
        if "cancelType" in data.keys():
            cancelType = data["cancelType"]
        if (cancelType == "casse" and out == data["cancel"]) or (cancelType == "content" and out.strip().lower() == data["cancel"].strip().lower()):
            value = -1
        else:
            value = out
    return value


def extractValueFromPingValue(ctx: commands.Context, out: discord.Message):
    if ctx.bot.intents.message_content:
        return out.content
    else:
        pingTag = "<@%s>" % ctx.bot.user.id
        notPingTag = "<!@%s>" % ctx.bot.user.id
        if out.content.startswith(pingTag):
            return out.content[len(pingTag):]
        elif out.content.startswith(notPingTag):
            return out.content[len(notPingTag):]
        else:
            return out.content
