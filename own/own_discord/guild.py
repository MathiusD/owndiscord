import discord

async def inviteFromGuild(guild:discord.Guild, bot:discord.ClientUser, reason:str = None, max_age:int = 3600):
    if guild.system_channel:
        invite = await inviteFromChan(guild.system_channel, bot, reason, max_age)
        if invite is not None:
            return invite
    for chan in guild.text_channels:
        invite = await inviteFromChan(chan, bot, reason, max_age)
        if invite is not None:
            return invite
    return None

async def inviteFromChan(chan:discord.TextChannel, bot:discord.ClientUser, reason:str = None, max_age:int = 3600):
    user = chan.guild.get_member(bot.user.id)
    if user:
        if chan.permissions_for(user).create_instant_invite is True:
            return await chan.create_invite(reason=reason, max_age = max_age)
    return None

async def getFirstChanWithAllowSendMsg(guild:discord.Guild, bot:discord.ClientUser):
    user = guild.get_member(bot.user.id)
    if user:
        for chan in guild.text_channels:
            if chan.permissions_for(user).send_messages is True:
                return chan
    return None