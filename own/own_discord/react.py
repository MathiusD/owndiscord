from discord.ext import commands
import discord, asyncio, logging

async def _prep_react(msg:discord.Message, react:str):
    haveIt = False
    for reaction in msg.reactions:
        if str(reaction) == react and reaction.me is True:
            haveIt = True
            break
    if haveIt is False:
        await msg.add_reaction(react)

def _define_check(ctx:commands.Context, msg:discord.Message, react:str, unlocked:bool = False):
    def check(payload:discord.RawReactionActionEvent):
        userValid = payload.user_id != ctx.bot.user.id if unlocked is True else payload.user_id == ctx.author.id
        checkIsValid = userValid and str(payload.emoji) == react and payload.message_id == msg.id
        logging.debug(
            "%s react with %s at message ID(%i) is %s [userValid:%s, unlocked:%s, reactWaiting:%s, initialMsg:%s]" % (
                payload.user_id, payload.emoji, payload.message_id,
                "valid" if checkIsValid else "wrong", userValid, unlocked, react,  msg.id
            )
        )
        return checkIsValid
    return check

async def wait_react(ctx:commands.Context, msg:discord.Message, react:str, timeout:float = 60.0, unlocked:bool = False):
    await _prep_react(msg, react)
    try:
        await ctx.bot.wait_for('raw_reaction_add', timeout=timeout, check=_define_check(ctx, msg, react, unlocked))
    except asyncio.TimeoutError:
        return False
    return True

async def wait_react_remove(ctx:commands.Context, msg:discord.Message, react:str, timeout:float = 60.0, unlocked:bool = False):
    await _prep_react(msg, react)
    try:
        await ctx.bot.wait_for('raw_reaction_remove', timeout=timeout, check=_define_check(ctx, msg, react, unlocked))
    except asyncio.TimeoutError:
        return False
    return True